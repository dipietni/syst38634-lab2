package Package;

public class Methods {

	public static void main(String[] args) {
		
//		int [] numbers = {45, 65, 25};
//		System.out.println("smallest: " + smallest(numbers));
//		System.out.println("average: " + average(numbers));
//		String test = "350";
//		System.out.println("middle: " + middle(test));
		System.out.println("vowels: " + vowels("w3resource"));
		System.out.println("getChar: " + getChar("Java Exercises!", 10));
		System.out.println("unicode: " + unicode("w3rsource.com"));
	}
	
	public static int smallest (int [] numbers) {
		int smallest = numbers[0];
		for (int i = 0 ; i < numbers.length - 1 ; i++) {
			if (numbers[i + 1] < smallest) {
				smallest = numbers[i+1];
			}
		}
		return smallest;
	}
	public static int average (int [] numbers) {
		int average = 0;
		int count;
		for (count = 0 ; count < numbers.length ; count++) {
			average += numbers[count];
		}
		average = average/count;
		return average;
		
	}
	public static String middle (String word) {
		String middleChars = null;
		int length = word.length();
		if (length % 2 == 1) {
			middleChars = Character.toString(
					word.charAt(length/2));
		} else {
			middleChars = Character.toString(
					word.charAt(length/2 - 1)) + 
					Character.toString(
							word.charAt(length/2));
		}
		return middleChars;
	}
	public static int vowels (String word) {
		int vowels = 0;
		
		for (int i = 0 ; i < word.length(); i++) {
			char ch = word.charAt(i);
			//y's will not be considered vowels even though 
			//sometimes they are
			if (ch == 'a' || ch == 'e' ||
					ch == 'i' || ch == 'o' || ch == 'u' ) {
				vowels++;
			}
		}
		return vowels;
	}
	public static char getChar (String word, int i) {
		return word.charAt(i);
	}
	public static int unicode (String word) {
		return word.length();
	}

}
